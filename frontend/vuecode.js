
let log = new Logger();


  /**
   * Global webservic connection object
   */
  let connection = false;

  /**
   * Global app instance, pointing to the App vue instance which holds the server data
   **/

  let app;

  // Map Pi error severity codes to bootstrap text colors
  const severityTextColors = {
      "Debug": "text-muted",
      "Info": "text-info",
      "Warn": "text-warning",
      "Error": "text-danger",
      "Alert": "text-danger",
  };

  new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
      message: 'Hello Vue.js!',
      // Server-generated data
        server: {
            msg: '',
            login: -1,
            rssi: 0,
            config: {},
            log: [],
        },

        // Client-generated data
        client: {
            connected: false,
            wsHost: location.host + '/websocket',
            ping: 0
        }
    },
    computed: {
        logSeverityColor: function() {
                c = severityTextColors[this.server.log[0].severity];
                return  c ? c : severityTextColors["Info"];
            }
        },
    methods: {
      connectWS() {
        try {
            connection = initWS("ws://" + this.client.wsHost);
            /*setInterval(function(){
                if (connection && connection.readyState === WebSocket.OPEN && connection.lastPingReceived + 3000 < Date.now()) {
                    log.w("Heartbeat timeout, reconnecting...", "client");
                    connection.close();
                    app.connectWS();
                }
            }, 5000);*/

        } catch (err) {
            log.e("Connection failed: " + err.message, "client");
        }
      }
    },
    watch: {
        "server.msg": function (val) {
            switch (val.substr(0, 2)) {
                case "w:":
                    log.w(val.substr(2), 'Pi');
                    break;
                case "i:":
                    log.i(val.substr(2), 'Pi');
                    break;
                case "e:":
                    log.e(val.substr(2), 'Pi');
                    break;
                case "c:":
                    log.a(val.substr(2), 'Pi');
                    break;

                default:
                    log.i(val, 'Server');
                    break;
            }
        }
    },
    created() {
        app = this;
        log.onEntry = function(entry) {
            app.server.log.unshift(entry);
            if (app.server.log.length > 100) app.server.log.pop();
        };
        this.syncToServerDebouncer = {};

        //if (Cookie.get("wsHost")) {
        //    this.client.wsHost = Cookie.get("wsHost");
        //}

        // Add custom initialisation here..
        // E.g., define vars that should be auto-synced to server:
        // this.syncToServer('server.red', 'r');
        // this.syncToServer('server.green', 'g');
        // this.syncToServer('server.blue', 'b');

        app.connectWS();
        },
  });

  //app.connectWS();

