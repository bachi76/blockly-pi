// Fetch values from inputs with shadow elements

function resolveValue(block, fieldName) {
    var val;
    if (block.getField(fieldName)) {
        // Internal number.
        val = String(parseInt(block.getFieldValue(fieldName), 10));
    } else {
        // External number.
        val = Blockly.Python.valueToCode(block, fieldName, Blockly.Python.ORDER_NONE) || '0';
    }
    return val;
}