Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_led_create",
        "message0": "LED %1 an Pin %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "input_value",
                "name": "PIN",
                "check": "Number"
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle eine LED.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_led_create'] = function(block) {
    return `led["${block.getFieldValue("NAME")}"] = PWMLED(${resolveValue(block, 'PIN')})\n`;
};