Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_button_create",
        "message0": "Button %1 an Pin %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "input_value",
                "name": "PIN",
                "check": "Number"
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen Button.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_button_create'] = function(block) {

    // bounce_time=0.smth breaks wait_for(), maybe related to https://github.com/RPi-Distro/python-gpiozero/issues/550
    return `button["${block.getFieldValue("NAME")}"]  = Button(${resolveValue(block, 'PIN')}, pull_up=False, hold_time=3)\n`;
};