Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_ads1115_create",
        "message0": "Analog Digital Converter ADS1115 mit Messbereich %1 verwenden",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "GAIN",
                "options": [
                    ["± 4.096V", "1"],
                    ["± 2.048V", "2"],
                    ["± 1.024V", "4"],
                    ["± 0.512V", "8"],
                    ["± 0.256V", "16"]
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen ADS1115 ADC",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_i2c_ads1115_create'] = function(block) {
    let cmd = `ads1115 = ADS.ADS1115(i2c)\n`;
    cmd +=`ads1115.gain = ${block.getFieldValue("GAIN")}\n`;
    cmd += `adc_a0 = AnalogIn(ads1115, ADS.P0)\n`;
    cmd += `adc_a1 = AnalogIn(ads1115, ADS.P1)\n`;
    cmd += `adc_a2 = AnalogIn(ads1115, ADS.P2)\n`;
    cmd += `adc_a3 = AnalogIn(ads1115, ADS.P3)\n`;
    return cmd;
};