Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_neopixel_create",
        "message0": "Neopixel mit %1 LEDs, Helligkeit %2% (Farbfolge %3)",
        "args0": [
            {
                "type": "input_value",
                "name": "LEDS",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "BRIGHTNESS",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "ORDER",
                "options": [
                    ["GRB", "GRB"],
                    ["RGB", "RGB"]
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen Neopixel-Strip",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.gcwawqu0jmde"
    }
]);


// Generator
Blockly.Python['gpio_neopixel_create'] = function(block) {
    return `neopixel = neopixel.NeoPixel(board.D10, ${resolveValue(block, 'LEDS')}, brightness=${resolveValue(block, 'BRIGHTNESS')/100}, auto_write=True, pixel_order=neopixel.${block.getFieldValue("ORDER")})\n`;
};