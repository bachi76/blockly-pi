Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_ultrasonic_distance",
        "message0": "Distanz Ultraschall-Sensor %1 in cm",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            }
        ],
        "output": "Number",
        "colour": 355,
        "tooltip": "Gibt die Distanz zu einem Objekt in cm zurück, oder 0 falls nichts in Reichweite ist",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.368ltgjp774g"
    }
]);

// Generator
Blockly.Python['gpio_ultrasonic_distance'] = function(block) {
    const code = `round(ultrasonic["${block.getFieldValue("NAME")}"].distance * 100)`;
    return [code, Blockly.Python.ORDER_NONE];
};