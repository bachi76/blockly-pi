Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_log",
        "message0": "Logge als %1: %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "SEVERITY",
                "options": [
                    ["Info", "info"],
                    ["Warnung", "warning"],
                    ["Fehler", "error"],
                ]
            },
            {
                "type": "input_value",
                "name": "TEXT"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Schreibe einen Logeintrag",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_log'] = function(block) {

    // bounce_time=0.smth breaks wait_for(), maybe related to https://github.com/RPi-Distro/python-gpiozero/issues/550
    return `logger.${block.getFieldValue("SEVERITY")}(${resolveValue(block, 'TEXT')})\n`;
};