Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_bme280_read",
        "message0": "Temp. von BME280 Sensor %1",
        "args0": [
             {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["Temperatur", "temperature"],
                    ["Luftfeuchtigkeit", "humidity"],
                    ["Luftdruck", "pressure"],
                ]
            },
        ],
        "output": "Float",
        "colour": 355,
        "tooltip": "BME280 Sensorwert auslesen",
        "helpUrl": ""
    }
]);

// Generator
Blockly.Python['gpio_i2c_bme280_read'] = function(block) {
    const code = `bme280.${block.getFieldValue("NAME")}`;
    return [code, Blockly.Python.ORDER_NONE];
};