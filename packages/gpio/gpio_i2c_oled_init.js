Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_oled_init",
        "message0": "Verwende OLED mit Auflösung %1",
        "args0": [
             {
                "type": "field_dropdown",
                "name": "RESOLUTION",
                "options": [
                    ["128x32", "128x32"],
                    ["128x64", "128x64"],
                ]
            },
        ],
        "colour": 355,
        "previousStatement": null,
        "nextStatement": null,
        "tooltip": "Ein SSD1306 OLED Display verwenden",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.18wdhahyyn98"
    }
]);

// Generator
Blockly.Python['gpio_i2c_oled_init'] = function(block) {
    let code = "";
    if (block.getFieldValue("RESOLUTION") === '128x32') {
        return "oled = OledText(i2c, 128, 32)\n";
    }
    if (block.getFieldValue("RESOLUTION") === '128x64') {
        return "oled = OledText(i2c, 128, 64)\n";
    }
};