Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_oled_text",
        "message0": "Auf OLED Zeile %2 ausgeben: %1",
        "args0": [
            {
                "type": "input_value",
                "name": "TEXT"
            },
            {
                "type": "input_value",
                "name": "LINE"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Text oder Icon auf OLED Display ausgeben.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.18wdhahyyn98"
    }
]);


// Generator
Blockly.Python['gpio_i2c_oled_text'] = function(block) {
    return `oled.text(str(${resolveValue(block, 'TEXT')}), ${resolveValue(block, 'LINE')})\n`;
};