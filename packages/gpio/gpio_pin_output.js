Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_pin_output",
        "message0": "Schalte Pin %1 %2",
        "args0": [
            {
                "type": "input_value",
                "name": "PIN",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "MODE",
                "options": [
                    ["ein", "GPIO.HIGH"],
                    ["aus", "GPIO.LOW"],
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Vorr der Verwendung müssen Pins initialisiert werden.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_pin_output'] = function(block) {
    return `GPIO.output(${resolveValue(block, 'PIN')}, ${block.getFieldValue("MODE")})\n`;
};