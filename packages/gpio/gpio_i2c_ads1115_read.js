Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_ads1115_read",
        "message0": "%1 von ADS Port %2",
        "args0": [
             {
                "type": "field_dropdown",
                "name": "TYPE",
                "options": [
                    ["Messwert", "value"],
                    ["Spannung (V)", "voltage"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "PORT",
                "options": [
                    ["A0", "0"],
                    ["A1", "1"],
                    ["A2", "2"],
                    ["A3", "3"]
                ]
            },
        ],
        "output": "Number",
        "colour": 355,
        "tooltip": "16 Bit Messwert (zw. -32768 und 32767) oder Spannung (V) vom ADC einlesen.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.87wubdcvmkhj"
    }
]);

// Generator
Blockly.Python['gpio_i2c_ads1115_read'] = function(block) {
    const code = `adc_a${block.getFieldValue("PORT")}.${block.getFieldValue("TYPE")}`;
    return [code, Blockly.Python.ORDER_NONE];
};