Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_pwm_hw",
        "message0": "PWM-Signal an Pin %1: %2 Hz, Duty-Cycle %3 %",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "PIN",
                "options": [
                    ["18 (PWM0)", "18"],
                    ["13 (PWM1)", "13"]
                ]
            },
            {
                "type": "input_value",
                "name": "FREQ",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "DUTY",
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Starte ein Pulsweitenmodulationssignal an einem der PWM-Pins.",
        "helpUrl": ""
    }
]);


// Generator
// TODO: Should we try GPIO Zero with a gpiozero.pins.pigpio.PiGPIOFactory pin factory instead?
Blockly.Python['gpio_pwm_hw'] = function(block) {
    let duty = Math.round(resolveValue(block, 'DUTY') / 100 * 1000000); // 0 - 1mio
    return `pig.hardware_PWM(${block.getFieldValue("PIN")}, ${resolveValue(block, 'FREQ')}, ${duty})\n`;
};