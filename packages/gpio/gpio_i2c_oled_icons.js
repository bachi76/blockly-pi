Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_oled_icons",
        "message0": "Icon %1",
        "args0": [
             {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "input_value",
                "name": "CODE",
                "check": "String"
            }
        ],
        "output": "String",
        "colour": 355,
        "tooltip": "Gibt ein Icon von FontAwesome auf dem OLED-Display aus.",
        "helpUrl": "https://fontawesome.com/cheatsheet/free/solid"
    }
]);

// Generator
Blockly.Python['gpio_i2c_oled_icons'] = function(block) {
    const code = `temp["${block.getFieldValue("NAME")}"].get_temperature()`;
    return [code, Blockly.Python.ORDER_NONE];
};