Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_rcswitch",
        "message0": "Funksteckdose %1 %2 %3",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "GROUP",
                "options": [
                    ["I", "1"],
                    ["II", "2"],
                    ["III", "3"],
                    ["IV", "4"],
                    ["00000", "00000"],
                    ["00001", "00001"],
                ]
            },
            {
                "type": "field_dropdown",
                "name": "DEVICE",
                "options": [
                    ["1", "1"],
                    ["2", "2"],
                    ["3", "3"],
                    ["4", "4"],
                    ["A", "1"],
                    ["B", "2"],
                    ["C", "3"],
                    ["D", "4"],
                    ["E", "5"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "MODE",
                "options": [
                    ["ein", "1"],
                    ["aus", "0"]
                ]
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Steuere eine 433Mhz Funksteckdose",
        "helpUrl": ""
    }
]);


// Generator - currently fixed group code
Blockly.Python['gpio_rcswitch'] = function(block) {
    return `rcswitch_send("${block.getFieldValue("GROUP")}", "${block.getFieldValue("DEVICE")}", "${block.getFieldValue("MODE")}")\n`;
};