Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_neopixel_set_hex",
        "message0": "Setze Neoxpixel %1 auf Hexwert #%2",
        "args0": [
            {
                "type": "input_value",
                "name": "LED",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "HEX"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Eine Neopixel-LED ansteuern.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.gcwawqu0jmde"
    }
]);


// Generator
Blockly.Python['gpio_neopixel_set_hex'] = function(block) {
    return `neopixel[${resolveValue(block, 'LED')}] = int((${resolveValue(block, 'HEX')}).strip("#"), 16)\n`;
};