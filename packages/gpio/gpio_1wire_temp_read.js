Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_1wire_temp_read",
        "message0": "Temp. von DS18B20 Sensor %1",
        "args0": [
             {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
        ],
        "output": "Float",
        "colour": 355,
        "tooltip": "Temperatur in Celsius von einem DS18B20 Sensor auslesen",
        "helpUrl": ""
    }
]);

// Generator
Blockly.Python['gpio_1wire_temp_read'] = function(block) {
    const code = `temp["${block.getFieldValue("NAME")}"].get_temperature()`;
    return [code, Blockly.Python.ORDER_NONE];
};