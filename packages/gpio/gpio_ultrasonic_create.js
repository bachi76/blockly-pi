Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_ultrasonic_create",
        "message0": "Distanzsensor (HC-SR04) %1. Pins: Trigger %2, Echo %3",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "input_value",
                "name": "TRIG",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "ECHO",
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen Ultraschall-Distanzsensor vom Typ HC-SR04",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.368ltgjp774g"
    }
]);


// TODO: Try https://gpiozero.readthedocs.io/en/stable/api_pins.html?highlight=pin_factory#changing-the-pin-factory
Blockly.Python['gpio_ultrasonic_create'] = function(block) {

    return `ultrasonic["${block.getFieldValue("NAME")}"]  = DistanceSensor(echo=${resolveValue(block, 'ECHO')}, trigger=${resolveValue(block, 'TRIG')}, queue_len=20, max_distance=3, threshold_distance=0.3, partial=False)\n`;
};