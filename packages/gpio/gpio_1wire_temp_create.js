Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_1wire_temp_create",
        "message0": "Temp.sensor DS18B20 %1 mit ID %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "input_value",
                "name": "ID",
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen DS18B20 Temperatursensor.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_1wire_temp_create'] = function(block) {
    return `temp["${block.getFieldValue("NAME")}"] = W1ThermSensor(W1ThermSensor.THERM_SENSOR_DS18B20, ${resolveValue(block, 'ID')})\n`;
};