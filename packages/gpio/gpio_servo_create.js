Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_servo_create",
        "message0": "Servo %1 an Pin %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "input_value",
                "name": "PIN",
                "check": "Number"
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen Button.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.x8mwov9kz589"
    }
]);


// TODO: Try https://gpiozero.readthedocs.io/en/stable/api_pins.html?highlight=pin_factory#changing-the-pin-factory
Blockly.Python['gpio_servo_create'] = function(block) {

    return `servo["${block.getFieldValue("NAME")}"]  = AngularServo(${resolveValue(block, 'PIN')}, initial_angle=0, min_angle=0, max_angle=180, min_pulse_width=0.4/1000, max_pulse_width=2.8/1000, frame_width=20/1000, pin_factory=None)\n`;
};