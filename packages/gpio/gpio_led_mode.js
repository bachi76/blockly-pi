Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_led_mode",
        "message0": "LED %1 %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "MODE",
                "options": [
                    ["ein", "on()"],
                    ["aus", "off()"],
                    ["umschalten", "toggle()"],
                    ["blinken", "blink()"],
                    ["pulsieren", "pulse()"]
                ]
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle eine LED.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_led_mode'] = function(block) {
    return `led["${block.getFieldValue("NAME")}"].${block.getFieldValue("MODE")}\n`;
};