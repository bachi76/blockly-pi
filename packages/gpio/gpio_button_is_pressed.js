Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_button_is_pressed",
        "message0": "Button %1 %2 gedrückt?",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "DURATION",
                "options": [
                    ["kurz", "short"],
                    ["lange", "long"]
                ]
            }
        ],
        "output": "Boolean",
        "colour": 355,
        "tooltip": "Gibt Wahr zurück, wenn der Button gedrückt wird, sonst False",
        "helpUrl": ""
    }
]);

// Generator
Blockly.Python['gpio_button_is_pressed'] = function(block) {
    let code;

    if (block.getFieldValue('DURATION') === "short") {
        code = `button["${block.getFieldValue('NAME')}"].is_pressed`;
    } else {
        code = `button["${block.getFieldValue('NAME')}"].is_held`;
    }

    return [code, Blockly.Python.ORDER_NONE];
};