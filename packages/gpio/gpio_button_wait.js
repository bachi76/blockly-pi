Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_button_wait",
        "message0": "Warte bis Button %1 %2 wird (%3 max. %4 Sek.)",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "EVENT",
                "options": [
                    ["gedrückt", "press"],
                    ["losgelassen", "release"]
                ]
            },
            {
              "type": "field_checkbox",
              "name": "TIMEOUT",
              "checked": false
            },
            {
                "type": "input_value",
                "name": "SEC",
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Wartet, bis eines der Ereignisse (oder das Timeout) eintrifft",
        "helpUrl": ""
    }
]);

// Generator
Blockly.Python['gpio_button_wait'] = function(block) {
    const name = block.getFieldValue('NAME');
    const event = block.getFieldValue('EVENT');
    const isTimeout = block.getFieldValue('TIMEOUT');
    const sec = resolveValue(block, 'SEC');
    const timeout = (isTimeout === "TRUE") ? "timeout=" + sec : "";

    return `button["${name}"].wait_for_${event}(${timeout})\n`;
};