Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_bme280_create",
        "message0": "Temperatursensor BME280 (an I2C-Addr. %1)",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "ID",
                "options": [
                    ["76", "0x76"],
                    ["77", "0x77"]
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Verwende den BME280 Temperatur-/Feuchtesensor.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_i2c_bme280_create'] = function(block) {
    return `bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c, ${block.getFieldValue("ID")})\n`;
};