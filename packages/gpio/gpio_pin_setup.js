Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_pin_setup",
        "message0": "Verwende Pin %1 als %2",
        "args0": [
            {
                "type": "input_value",
                "name": "PIN",
                "check": "Number"
            },
            {
                "type": "field_dropdown",
                "name": "MODE",
                "options": [
                    ["Ausgabe", "GPIO.OUT"],
                    ["Eingabe", "GPIO.IN"],
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Vor der Verwendung müssen Pins initialisiert werden.",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_pin_setup'] = function(block) {

    // We use the input pull-down resistor to simplify the usage
    if (block.getFieldValue("MODE")==="GPIO.IN") {
        return `GPIO.setup(${resolveValue(block, 'PIN')}, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)\n`;
    } else {
        return `GPIO.setup(${resolveValue(block, 'PIN')}, GPIO.OUT)\n`;
    }
};