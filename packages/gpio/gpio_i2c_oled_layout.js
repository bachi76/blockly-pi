Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_i2c_oled_layout",
        "message0": "Verwende OLED-Layout %1",
        "args0": [
             {
                "type": "field_dropdown",
                "name": "LAYOUT",
                "options": [
                    ["64px: 5 Zeilen", "Layout64.layout_5small()"],
                    ["64px: 1 grosse, 3 kleine Zeilen", "Layout64.layout_1big_3small()"],
                    ["64px: 1 Titelzeile", "Layout64.layout_1big_center()"],
                    ["64px: Grosses Icon", "Layout64.layout_icon_only()"],
                    ["64px: 3 Zeilen links, 3 Icons rechts", "Layout64.layout_3medium_3icons()"],
                    ["64px: Icon links, 1 grosse, 2 kleine Zeilen rechts", "Layout64.layout_icon_1big_2small()"],
                    ["32px: 1 grosse Zeile", "Layout32.layout_1big()"],
                    ["32px: 2 mittlere Zeilen", "Layout32.layout_2medium()"],
                    ["32px: 3 kleine Zeilen", "Layout32.layout_3small()"],
                    ["32px: 1 grosse, 1 kleine Zeile", "Layout32.layout_1big_1small()"],
                    ["32px: Icon links, Text rechts", "Layout32.layout_iconleft_1big()"],
                    ["32px: Icon rechts, Text links", "Layout32.layout_iconright_1big()"],
                ]
            },
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Ein Display-Layout setzen.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.18wdhahyyn98"
    }
]);

// Generator
Blockly.Python['gpio_i2c_oled_layout'] = function(block) {
    return `oled.layout = ${block.getFieldValue("LAYOUT")}\n`;
};