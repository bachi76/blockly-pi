Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_time_sleep",
        "message0": "Warte %1 Sekunden",
        "args0": [
            {
                "type": "input_value",
                "name": "SEC",
                "check": "Number"
            },
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    }
]);


// Generator
Blockly.Python['gpio_time_sleep'] = function(block) {
    return `sleep(${resolveValue(block, 'SEC')})\n`;
};