Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_servo_set",
        "message0": "Servo %1 auf %2 %3 stellen",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "NAME",
                "options": [
                    ["A", "A"],
                    ["B", "B"],
                    ["C", "C"],
                    ["D", "D"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "POS",
                "options": [
                    ["Maximum", "max()"],
                    ["Minimum", "min()"],
                    ["Mitte", "mid()"],
                    ["Winkel", "angle"],
                    ["aus", "detach()"]
                ]
            },
            {
                "type": "input_value",
                "name": "ANGLE",
                "check": "Number"
            }

        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Erstelle einen Button.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.x8mwov9kz589"
    }
]);


// TODO: Try https://gpiozero.readthedocs.io/en/stable/api_pins.html?highlight=pin_factory#changing-the-pin-factory
Blockly.Python['gpio_servo_set'] = function(block) {

    switch (block.getFieldValue("POS")) {
        case "angle":
            return `servo["${block.getFieldValue("NAME")}"].angle = ${resolveValue(block, 'ANGLE')}\n`;
            break;

        default:
            return `servo["${block.getFieldValue("NAME")}"].${block.getFieldValue("POS")}\n`;
    }
};