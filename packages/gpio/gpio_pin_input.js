Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_pin_input",
        "message0": "Ist an Pin %1 Spannung angelegt?",
        "args0": [
            {
                "type": "input_value",
                "name": "PIN",
                "check": "Number"
            }
        ],
        "output": "Boolean",
        "colour": 355,
        "tooltip": "Vor der Verwendung müssen Pins initialisiert werden.",
        "helpUrl": ""
    }
]);

// Generator
Blockly.Python['gpio_pin_input'] = function(block) {
    const code = `GPIO.input(${resolveValue(block, 'PIN')})`;
    return [code, Blockly.Python.ORDER_NONE];
};