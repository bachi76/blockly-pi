import logging
import subprocess

import RPi.GPIO as GPIO

from gpiozero.pins.pigpio import PiGPIOFactory
from gpiozero import PWMLED, Button, DistanceSensor, Device, AngularServo

from w1thermsensor import W1ThermSensor

import pigpio

import board
import busio
import adafruit_bme280
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import neopixel

# OLED 1306
from oled_text import OledText, Layout64, Layout32

from time import sleep
from signal import pause

'''
Turn an rf controlled power switch on/off. Requires https://github.com/ninjablocks/433Utils
and the 'send' utility to be copied to /usr/bin/rcswitch-send
'''
def rcswitch_send(group_code, device_code, mode):
	subprocess.call(["rcswitch-send", group_code, device_code, mode])

'''
Use the pigpio lib for faster pin handling. Without it, PWM e.g. for servos, becomes
unprecise, resulting in servo jittering.
Needs the pigpio daemon running (sudo pigpiod - usually started via cron)
'''
Device.pin_factory = PiGPIOFactory()

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("blockly-pi")
logger.setLevel(logging.DEBUG)

i2c = busio.I2C(board.SCL, board.SDA)

GPIO.setmode(GPIO.BCM)
button = {}
led = {}
servo = {}
ultrasonic = {}
temp = {}

# We use the pigpio library for servo and pwm control. It requires a daemon that should run
# 'sudo pigpiod'
# http://abyz.me.uk/rpi/pigpio/python.html#hardware_PWM
pig = pigpio.pi()

logger.info("Programm gestartet!")


