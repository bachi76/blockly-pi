Blockly.defineBlocksWithJsonArray([
    {
        "type": "gpio_neopixel_set",
        "message0": "Setze Neoxpixel %1 auf %2 rot, %3 grün, %4 blau.",
        "args0": [
            {
                "type": "input_value",
                "name": "LED",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "R",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "G",
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "B",
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "Eine Neopixel-LED ansteuern.",
        "helpUrl": "https://docs.google.com/document/d/1G_A3cNzAohRZtJpBXlb9xGG4zNAH3jWeYTGIznhh3Jw/edit#heading=h.gcwawqu0jmde"
    }
]);


// Generator
Blockly.Python['gpio_neopixel_set'] = function(block) {
    return `neopixel[${resolveValue(block, 'LED')}] = (${resolveValue(block, 'R')}, ${resolveValue(block, 'G')}, ${resolveValue(block, 'B')})\n`;
};