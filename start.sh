#!/bin/sh

echo "Starting Blockly-Pi Webserver ..."
# FIXME: Started like this will cause a 90s dely on shutdown. Add that line directly to cron @reboot and there's no delay
sudo pigpiod -l -s 10
export GPIOZERO_PIN_FACTORY=pigpio
cd /home/pi/blockly-pi/
python3 server.py

