import html
import importlib
import json
import locale
import os
import pathlib
from datetime import datetime
import configparser
import threading
import time
from subprocess import Popen, PIPE, STDOUT
from queue import Queue
from logging.handlers import QueueHandler

import re
from bottle import debug, route, run as run, template, post, request, static_file, HTTPResponse, hook, abort, Bottle
import logging

from gevent.pywsgi import WSGIServer
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler
from gevent import sleep as gsleep

app = Bottle()

# Bottle Debug mode: Hot-reloads templates (defunct?)
app.debugMode = True

generated_code = None
execution_thread = None
process = None

default_module_name = "generated_source_code"
FOLDER_GENERATED_CODE = "generated_code"
FOLDER_CODEFILES = './data/block-codefiles/'
PID_PATH = './running-script.txt'

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("server")
logger_bpi = logging.getLogger("blockly-pi")
logger.setLevel(logging.INFO)
logger_bpi.setLevel(logging.DEBUG)

log_queue = Queue(maxsize=200)
queue_handler = QueueHandler(log_queue)
logger.addHandler(queue_handler)
logger_bpi.addHandler(queue_handler)


# To ensure utf-8 is set as default encoding. Or do it system-wide (https://www.cyberciti.biz/faq/how-to-set-locales-i18n-on-a-linux-unix/)
# locale.setlocale(locale.LC_ALL, 'de_CH.UTF-8')
locale.setlocale(locale.LC_ALL, 'en_GB.UTF-8')

'''
Process the stdout of the currently running generated code process (threaded)
'''
def read_proc_stdout(process, logger_bpi):

	logger_bpi.info("Program loading...")

	for line in process.stdout:

		# Need to re-map text output to log levels
		parts = line.split(":", 2)

		if len(parts) == 3:
			if parts[0] == "CRITICAL":
				logger_bpi.critical(parts[2])
			elif parts[0] == "ERROR":
				logger_bpi.error(parts[2])
			elif parts[0] == "WARNING":
				logger_bpi.warning(parts[2])
			elif parts[0] == "INFO":
				logger_bpi.info(parts[2])
			elif parts[0] == "DEBUG":
				logger_bpi.debug(parts[2])
			else:
				logger_bpi.info(line)
		else:
			logger_bpi.info(line)

	logger_bpi.info("Program ended.")
	os.remove(PID_PATH)


@app.hook('before_request')
def check_access():
	remoteaddr = request.environ.get('REMOTE_ADDR')
	forwarded = request.environ.get('HTTP\_X\_FORWARDED_FOR')

	if len(ip_whitelist) == 0 or (remoteaddr in ip_whitelist) or (forwarded in ip_whitelist):
		return
	else:
		abort(403, 'Nothing here for you.')


# Get all files of the extension folder
def get_codefiles():
	dir = pathlib.Path(FOLDER_CODEFILES)
	return [x for x in dir.glob("*.xml")]


@app.route('/')
def action_index():

	# Collect the list of all blocks to load

	blocks = []
	for cat in custom_block_categories:
		folder = "packages/{}".format(cat)
		for file in os.listdir(folder):
			if file.endswith(".js"):
				blocks.append(os.path.join(folder, file))

	data = {
		'codefiles': [n.name.replace('.xml', '') for n in get_codefiles()],
		'selectedfile': request.GET.get('file'),
		'categories': custom_block_categories,
		'blocks': blocks,
		'host': request.get_header('host').split(":")[0]
	}

	return template('index', data)


@app.post('/run/<filename:re:[a-zA-Z0-9_-]*>')
@app.post('/run')
def action_run(filename=default_module_name):
	global generated_code, execution_thread, process
	postdata = request.body.read().decode('utf-8')

	filepath = FOLDER_GENERATED_CODE + "/" + filename + ".py"
	logger.info("Filepath: " + filepath)

	try:
		os.rename(filepath, filepath + "_bak")
	except:
		pass

	try:
		# Replace unicode sequences that got escaped, e.g. \uf123 becomes \\uf123 on the way. Replace back to \uf123.
		unicode_chars = [x.group() for x in re.finditer(r'\\\\u\w{4}', postdata)]
		for u in unicode_chars:
			postdata = postdata.replace(u, chr(int(u[-4:], 16)))
			print(u)

		code = template('generated_source_code',
						created=datetime.now(),
						code=postdata,
						categories=custom_block_categories)

		code = html.unescape(code)

	except Exception as e:
		logger.error(e)
		return HTTPResponse(status=500, body=e)

	with open(filepath, "w") as code_file:
		code_file.write(code)

	# Init environment
	try:
		action_stop()
		run_script(filepath)

		# Save the started script path so it can be restarted upon reboot
		pid = open(PID_PATH, 'w')
		pid.write(filepath)
		pid.close()

	except Exception as e:
		logger.error(e)
		print(e)
		return HTTPResponse(status=500, body=e)

	return 'ok'


def run_script(filepath):
	"""Run a saved python script in its own process."""
	global execution_thread, process
	cmd = ["python3", filepath]
	process = Popen(cmd, stdout=PIPE, stderr=STDOUT, bufsize=1, universal_newlines=True)

	execution_thread = threading.Thread(target=read_proc_stdout, daemon=True, args=(process, logger_bpi))
	execution_thread.start()


@app.route('/stop')
def action_stop():
	global execution_thread, process
	if process is not None and process.poll() is None:
		logging.debug("Stopping running code process with pid {} ".format(process.pid))
		process.kill()
		execution_thread.join()
		return 'ok'


@app.route('/websocket')
def action_handle_websocket():
	""" Open the websocket connection """
	# FIXME: If > 1 client is connected, they're stealing each other log events
	global log_queue
	wsock = request.environ.get('wsgi.websocket')
	if not wsock:
		abort(400, 'Expected WebSocket request.')

	try:
		while True:
			if not log_queue.empty():
				l = log_queue.get_nowait()  # type: logging.LogRecord
				severity_prefix = l.levelname[0].lower()+":"
				dic = {"msg": severity_prefix + l.getMessage()}
				out = json.dumps(dic)
				print("WS send: " + out)
				wsock.send(out)

			gsleep(0.1)

	except WebSocketError as e:
		logger.warning("Webservice client disconnected")
		pass


@app.route('/block-codefiles/<filename:re:[a-zA-Z0-9_-]*>')
def action_load_block_codefile(filename):
	print('Trying to load block xml from: {}'.format(filename))
	return static_file(filename + '.xml', root=FOLDER_CODEFILES)


@app.post('/block-codefiles/<filename:re:[a-zA-Z0-9_-]*>')
def action_save_block_codefile(filename):
	postdata = request.body.read().decode('utf-8')
	print(postdata)
	# code = html.unescape(postdata)
	filepath = './data/block-codefiles/' + filename + '.xml'
	with open(filepath, "w") as code_file:
		code_file.write(postdata)
		logger.info("Written new blockly code file to {}".format(filepath))

	return 'ok'
# TODO: Error handling

@app.route('/shutdown')
def action_shutdown():
	logger.info("Shutting down the Pi via webinterface.")
	Popen("sleep 2; sudo shutdown now", shell=True)

	return "<html><h1>Goodbye.</h1></html>"


# Tool to change spreadshet copied data to readable text (tab to dot)
@app.route('/textumwandler')
def action_textumwandler():
	return server_static('tab_to_char.html')


@app.route('/packages/<filepath:path>')
def action_server_static(filepath):
	return static_file(filepath, root='./packages/')


@app.route('/<filepath:path>')
def action_server_static(filepath):
	return static_file(filepath, root='./frontend/')


# Load config
config = configparser.ConfigParser()
config.read("config.ini")
ip_whitelist = json.loads(config.get('Server', 'ip_whitelist'))
custom_block_categories = json.loads(config.get('Server', 'custom_block_categories'))

# Restart a previously running python script
if os.path.isfile(PID_PATH):
	fp = open(PID_PATH, 'r')
	filepath = fp.read()
	fp.close()
	try:
		logger.info("Re-start previously running script '{}'".format(filepath))
		run_script(filepath)
	except:
		logger.warning("Could not start previously run python script '{}'".format(filepath))

# Start the server
# Allow all (and restrict using ip_whitelist if needed)
host = '0.0.0.0'
port = json.loads(config.get('Server', 'port'))

logger.info("Server started on port {}".format(port))
if len(ip_whitelist) > 0:
	logger.info("Access restricted to {}".format(ip_whitelist))
server = WSGIServer((host, port), app, handler_class=WebSocketHandler)
server.serve_forever()
