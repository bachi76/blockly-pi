<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="google" value="notranslate">
  <title>Blockly Pi</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <link rel="stylesheet" href="style.css">
  <script src="storage.js"></script>
  <script src="blockly/blockly_compressed.js"></script>
  <script src="blockly/blocks_compressed.js"></script>
  <script src="blockly/javascript_compressed.js"></script>
  <script src="blockly/python_compressed.js"></script>
  <script src="blockly/php_compressed.js"></script>
  <script src="blockly/lua_compressed.js"></script>
  <script src="blockly/dart_compressed.js"></script>

  <script src="https://unpkg.com/vue"></script>
  <script src="logger.js"></script>
  <script src="websocket.js"></script>

  <!-- Helpers -->
  <script src="support.js"></script>

  <!-- Custom blocks -->
% for block in blocks:
  <script src="{{block}}"></script>
% end

  <script src="code.js"></script>

  <style>
    body {tab-size: 4}
  </style>
</head>
<body>

<div id="app" style="height: 100vh;">


<table width="100%" height="100%">
  <tr>
    <td>
      <h1>
        Blockly Pi
      </h1>
    </td>
    <td class="farSide">
      <div v-if="server.log[0]" v-bind:class="logSeverityColor" title="Letzte Log-Zeile vom Pi. Um alle Logeinträge zu sehen: Entwicklertools öffnen (CTRL-Shift + i) und 'Console' Tab auswählen.">Pi [[server.log[0].severity]]: [[server.log[0].msg]] &nbsp;</div>

      <!-- Hidden until we have translations.--> <select id="languageMenu" hidden></select>
    </td>
  </tr>
  <tr>
    <td colspan=2>
      <table width="100%">
        <tr id="tabRow" height="1em">
          <td id="tab_blocks" class="tabon">...</td>
          <td class="tabmin">&nbsp;</td>
          <td id="tab_python" class="taboff">Python</td>
          <td class="tabmin">&nbsp;</td>
          <td id="tab_tools" class="taboff">Hilfsmittel</td>
          <td class="tabmin">&nbsp;</td>
          <td id="tab_xml" class="taboff">XML</td>
          <td class="tabmax">

            <div class="container-fluid farSide" style="display: flex; justify-content: flex-end">
              <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                  <div class="btn-group mr-2" role="group" aria-label="First group">

                      <button id="runButton" type="submit" class="btn btn-sm btn-danger"><span class="fa fa-play"></span></button>
                      <button id="stopButton" type="submit" title="Das Programm anhalten." class="btn btn-sm btn-danger"><span class="fa fa-stop"></span></button>
                  </div>

                  <div class="btn-group mr-2" role="group" aria-label="Second group">
                      <button id="saveButton" title="Das Programm speichern." type="submit" class="btn btn-sm btn-primary"><span class="fa fa-save"></span></button>

                    <select id="loadMenu" title="Ein gespeichertes Programm laden. Nicht gespeicherte Änderungen am aktuellen Programm gehen verloren!" class="form-control form-control-sm">
                      <option></option>
                      % for codefile in codefiles:
                        <option {{!'selected="selected"' if codefile == selectedfile else ""}} >{{codefile}}</option>
                      % end
                    </select>&nbsp;

                    <button type="submit" class="btn btn-sm btn-primary"><span class="fa fa-folder-open"></span></button>
                  </div>

                  <div class="btn-group mr-2" role="group" aria-label="Third group">
                      <a href="ssh://pi@{{host}}" target="shell"><button type="submit" title="Terminlaverbindung per SSH zum Pi öffnen. Benötigt die Chrome SSH-Erweiterung (s. Hilfsmittel)." class="btn btn-sm btn-secondary"><span class="fa fa-terminal"></span></button></a>
                      <a href="shutdown" onclick="return confirm('Wirklich den Raspberry Pi Zero ausschalten? Was nicht gespeichert ist, geht verloren!')"><button type="submit" title="Den Pi ausschalten." class="btn btn-sm btn-secondary"><span class="fa fa-power-off"></span></button></a>
                  </div>
              </div>
            </div>

<!--
            <button id="trashButton" class="notext" title="...">
              <img src='blockly/media/1x1.gif' class="trash icon21">
            </button>
            <button id="linkButton" class="notext" title="...">
              <img src='blockly/media/1x1.gif' class="link icon21">
            </button>
            <button id="saveButton" class="notext" title="...">
              <img src='blockly/media/1x1.gif' class="save icon21">
            </button>
            <button id="runButton" class="notext primary" title="...">
              <img src='blockly/media/1x1.gif' class="run icon21">
            </button>
-->
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td height="99%" colspan=2 id="content_area">
    </td>
  </tr>
</table>
<div id="content_blocks" class="content"></div>
<pre id="content_python" class="content"></pre>
<div id="content_tools" class="content">
  % include('tab-resources.tpl.html')
</div>


<textarea id="content_xml" class="content" wrap="off"></textarea>

<!--  End Vue diff -->
</div>

<script src="vuecode.js"></script>

<xml id="toolbox" style="display: none">

%# Import custom block categories
% for cat in categories:
%   include("packages/"  + cat + "/categories.xml")
% end

</xml>


</body>
</html>
