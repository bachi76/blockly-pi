"""
Blockly Pi Control Script
Created: {{created}}

Generator: Blockly for Pi
"""

%# Import custom category init code
% for cat in categories:

# Init for {{cat}}
% include("packages/"  + cat + "/init.py")
% end

# Blockly-generated code start

{{code}}

# Blockly-generated code end
